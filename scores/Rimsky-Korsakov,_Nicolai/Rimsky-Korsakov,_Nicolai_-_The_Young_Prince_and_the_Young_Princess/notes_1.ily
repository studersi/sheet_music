\time 3/8
\partial 4
\key g \major

\repeat volta 2 {
	b8\mp (c) |
	d8. (e16 d8) |
	b8 (a g) |
	b4.
	(b8) b\< (c) |
	d8\! (e16 d e d) |
	c16 (b c b a g) |
	b4.\>

	(e,8\!) e (g) |
	a8. (b16 a8) |
	g8 (fis g) |
	e4.
	(e8) g (fis) |
	e8 (fis g) |
	a8 (b16 a g fis) |
	g4.\>
	(g8\!)
}
