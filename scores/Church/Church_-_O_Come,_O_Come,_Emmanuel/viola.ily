\new Staff \with {
	\include "../../../util/lilypond/styles/staff_viola.ily"
}{
	\relative c {
		\clef alto

		\include "./notes_1.ily"
	}
}
\new Staff \with {
	\include "../../../util/lilypond/styles/staff_viola.ily"
}{
	\relative c' {
		\clef alto

		\include "./notes_2.ily"
	}
}
