INPUT_DIR := "scores"
OUTPUT_DIR := "dist"

SOURCES_ABC := $(shell find $(INPUT_DIR)/ -name "*.abc")
SOURCES_LILYPOND := $(shell find $(INPUT_DIR)/ -name "*.ly")

PDF_FILES := $(patsubst %.abc, %.pdf, $(SOURCES_ABC)) $(patsubst %.ly, %.pdf, $(SOURCES_LILYPOND))
MIDI_FILES := $(patsubst %.abc, %.midi, $(SOURCES_ABC)) $(patsubst %.ly, %.midi, $(SOURCES_LILYPOND))
MP3_FILES := $(patsubst %.abc, %.mp3, $(SOURCES_ABC)) $(patsubst %.ly, %.mp3, $(SOURCES_LILYPOND))
WAV_FILES := $(patsubst %.abc, %.wav, $(SOURCES_ABC)) $(patsubst %.ly, %.wav, $(SOURCES_LILYPOND))
ALL_FILES := $(PDF_FILES) $(MIDI_FILES) $(MP3_FILES) # $(WAV_FILES)

all: $(ALL_FILES)

pdf: $(PDF_FILES)

midi: $(MIDI_FILES)

mp3: $(MP3_FILES)

wav: $(WAV_FILES)

%.ps: %.abc
	abcm2ps -O "$@" "$<"

%.pdf: %.ps
	ps2pdf -sPAPERSIZE=a4 -sOutputFile="$@" "$<"

%.midi: %.abc
	abc2midi "$<" -o "$@"

%.pdf: %.ly
	lilypond --pdf -o "$(shell dirname $@)" "$<"

%.midi: %.ly
	lilypond --pdf -o "$(shell dirname $@)" "$<"

%.mp3: %.midi
	$(shell timidity -Ow -o - "$<" | lame - "$@")

%.wav: %.midi
	timidity $< -Ow -o $@

clean:
	find $(INPUT_DIR)/ -name "*.mid" -exec rm {} \;
	find $(INPUT_DIR)/ -name "*.midi" -exec rm {} \;
	find $(INPUT_DIR)/ -name "*.pdf" -exec rm {} \;
	find $(INPUT_DIR)/ -name "*.ps" -exec rm {} \;
	find $(INPUT_DIR)/ -name "*.mp3" -exec rm {} \;
	find $(INPUT_DIR)/ -name "*.wav" -exec rm {} \;
	rm -rf $(OUTPUT_DIR)/

dist: all
	mkdir -p $(OUTPUT_DIR)
	cp -R $(INPUT_DIR)/* $(OUTPUT_DIR)

pipeline:
	gitlab-runner exec docker dist

.PHONY: all pdf midi mp3 wav clean dist pipeline
