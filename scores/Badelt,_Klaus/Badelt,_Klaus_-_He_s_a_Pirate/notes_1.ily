\time 6/8
\key a \major
\tempo Briskly 4=120

\compressEmptyMeasures
\override MultiMeasureRest.expand-limit = #2

fis4 fis8 fis 4 fis8 |
fis 4 fis 8 fis fis fis |
fis4 fis8 fis 4 fis8 |
fis 4 fis 8 fis fis fis |
\break

fis4 fis8 fis 4 fis8 |
fis4 fis8 fis8 cis8 e |

\repeat volta 2 {
	fis4-> fis8 (fis8) fis8 gis8 |
	a4 a8 (a8) a b |
	\break

	gis4 gis8 (gis8) fis e |
	e8 fis r8 r8 cis8\downbow e |
	fis4-> fis8 (fis8) fis gis |
	a4 a8 (a8) a b |
	\break

	gis4 gis8 (gis8) fis e |
	fis8 r8 r8 r8 cis8\downbow e |
	fis4-> fis8 (fis8) fis a |
	b4 b8 (b8) b cis |
	\break

	d4 d8 (d8) cis b |
	cis8 fis, r8 r8 fis8\downbow gis |
	a4-> a8 (a8) b4 |
	cis8-> fis,-> r8 r8 fis8\downbow a |
	\break
}
\alternative {{
	gis4-> gis8-> (gis8) a fis |
	gis8-> r8 r8 r8 cis,8\downbow e |
}{
	gis4-> gis8-> (gis8) fis eis |
}}
fis4->\downbow \breathe fis8->\downbow (fis) gis4->\upbow |
\break

a4->\downbow \breathe a8->\downbow (a8) b4->\upbow |
cis4.-> r8 a8\downbow fis |
cis4. r4. |
d'4.->\downbow r8 b8\downbow fis |
\break

d4. r4. |
d4 e4 eis4 |
r4. r8 eis8 fis | \bar "||"
cis'4->\downbow \breathe cis8->\downbow (cis8) cis4->\upbow |
\break

d8-> cis-> r8 r4. |
b4->\downbow \breathe b8->\downbow (b8) b4-> |
b8-> cis-> r8 r4. |
cis4->\downbow \breathe cis8->\downbow (cis8) cis4-> |
\break

d8-> cis-> r8 r4. |
b4->\downbow a8\upbow (a8) gis4\downbow |
fis4\upbow r8 r8 fis8\downbow gis | \bar "||"
a4. (a8) b\upbow cis\upbow |
\break

b4\downbow a8 (a8) gis4 |
a4\upbow b8 (b8) cis4 |
b4.\downbow (b8) a\upbow b\upbow |
cis4.\downbow (cis8) b a |
\break

gis4\upbow a8 (a8) gis4 |
fis4\downbow fis8 (fis8) gis e |
fis4. (fis8) fis\upbow gis\upbow |
a4. (a8) gis a |
\break

b4\upbow a8 (a8) b4 |
cis4\downbow b8 (b8) a4 |
fis4.\upbow (fis8) fis\downbow gis\upbow |
a4 b8 (b8) cis4 |
\break

d4\upbow fis,8 (fis8) b4 |
a4.\downbow b8 gis8 r8 | \bar "||"
fis4.\downbow r4. |
cis'4.->\downbow r4. |
\break

d4.->\downbow r4. |
cis4->\downbow \breathe cis8->\downbow (cis8) cis4->\upbow |
cis8->\downbow b8-> r8 r4. |
b4.->\downbow r4. |
\break

a4.->\downbow r4. |
gis4->\downbow \breathe a8->\downbow (a8) gis4->\upbow |
gis8->\downbow fis-> r8 fis8 gis a |
cis4.-> fis,8 gis8 a |
\break

d4.-> fis,8 gis a |
cis4->\downbow \breathe cis8->\downbow (cis8) e4-> |
cis8-> b-> r8 r4. |
b4.->\downbow r4. |
\break

a4.->\downbow r4. |
gis4->\downbow \breathe a8->\downbow (a8) gis4-> |
fis4.-> r4. |

\bar "|."
