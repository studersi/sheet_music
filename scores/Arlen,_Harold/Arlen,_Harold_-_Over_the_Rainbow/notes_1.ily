\time 4/4
\key c \major
\tempo Adagietto 4=84

\compressEmptyMeasures
\override MultiMeasureRest.expand-limit = #2

R1*4 | \bar "||"
c2\mf\downbow c'2 |
b4 g8 a b4 c |
c,2\upbow a'2 |
\break

g1 |
a,2\downbow f'2 |
e4 c8 d e4 f4 |
d4\upbow b8 c d4 e4 | c2 r2 |
\break

c2\downbow c'2 |
b4 g8 a b4 c4 |
c,2\upbow a'2 |
g1 |
\break

a,2\downbow f'2 |
e4 c8 d e4 f4 |
d4\upbow b8 c d4 e4 |
c2. r8 g'8\upbow | \bar "||"
\break

e8\downbow g e g e8 g e g |
f8 g f g f8 g f g |
a2 a2\upbow |
a2. r8 g8\upbow |
\break

e8\downbow g e g e8 g e g |
fis8 g fis g fis8 g fis g |
b2 b2 |
d2 a2 | \bar "||"
\break

c,2\downbow c'2 |
b4 g8 a b4 c4 |
c,2 a'2 |
g1 |
a,2\downbow f'2 |
\break

e4 c8 d e4 f4 |
d4 b8c d4 e4 |
c2. r4 |
R1*3 |
r2 r4 r8 g'8\upbow |
\break

e8 g e g e8 g e g |
f8 g f g f8-"rit." g\< a b\! |
c1\f |
r1\fermata |

\bar "|."
