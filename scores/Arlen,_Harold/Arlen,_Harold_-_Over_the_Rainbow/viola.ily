\new Staff \with {
	\include "../../../util/lilypond/styles/staff_viola.ily"
}{
	\transpose g d \relative c' {
		\clef alto

		\include "./notes_1.ily"
	}
}
