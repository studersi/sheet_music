\time 4/4
\key f \major
\tempo Song


\partial 4

f,8 e |

\repeat volta 2 {
	d4 d d c8 d |
	f4 f g f8 g |
	a4 g8 f d4 d |
	\break

	c2. f8 e |
	d4 d d c8 d |
	f4 f g f8 g |
	\break

	a4 g8 f d4 c |
	d2. a'4 |
	c4 a a g8 f |
	\break

	g4 g g f8 g |
	a4 g8 f d4 d |
	c2. f8 e |
	\break

	d4 d d c8 d |
	f4 f g f8 g |
	a4 g8 f d4 c |
	\break

	d2. a'4 | \bar "||"
	c4 a a g8 f |
	g4 g g f8 g |
	\break

	a4 g8 f d4 d |
	c2. f8 e |
	d4 d d c8 d |
	\break

	f4 f g f8 g |
	a4 g8 f d4 c |
	d2.
}

\bar "|."
