\time 3/4
\key a \major

\repeat volta 2 {
	cis2\mf (d4) |
	d2 (cis4) |
	cis4 (b d) |
	d2 (cis4) |
	cis4 (b d) |
	d2 (cis4) |
	\break
}
\alternative {{
	cis2.
	(cis4) r4 r |
}{
	e2.
	(e4) r4 r |
}}

\repeat volta 2 {
	e2 (fis4) |
	fis2 (e4) |
	e,2 (fis4) |
	\break

	e'2 (fis4) |
	fis2 (e4) |
	e,2.
	(e4) r4 r |
}

\repeat volta 2 {
	cis'2 (d4) |
	d2 (cis4) |
	cis4 (b4 d) |
	\break

	d2 (cis4) |
	cis4 (b d) |
	d2 (cis4) |
}
\alternative {{
	cis2.
	(cis4) r4 r |
}{
	a2.
	(a2) r4 |
}}
\bar "|."
