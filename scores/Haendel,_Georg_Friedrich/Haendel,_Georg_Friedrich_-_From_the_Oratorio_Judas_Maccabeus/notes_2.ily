\repeat volta 2 {
	b2 g4. (a8) |
	b2 b, |
	d8 (g a b) a4 g |
	d1 |

	g8 (a b c) b4\tenuto (b4\tenuto) |
	b,2 g'2 |
	a4 (g) d4 (c) |
	b2. r4 |
}
g'8 (fis g a) g4 (e) |

fis4 dis e (b) |
a8 (a') g4 (fis e) |
b1 |
e8 (dis e fis) e4 d |
cis4 (a) e' (g) |

fis4 (g8 fis) e4 (<e a,>) |
<fis' d>2. r4 |
<b g,>2 <g g,>4. (a8) |
<b g,>2 <g b,>2 |
<fis d>8 (g a b) <a d,>4 <g d>4 |

<fis d>1 |
<g g,>8 (<a g,> b c) <b g,>4\tenuto (<b g,>\tenuto) |
<g b,>2 <b g,>2 |
<a d,>4 (<g d>4) <fis d>4 (<f c>4) |
<g b,>1 |
\bar "|."
