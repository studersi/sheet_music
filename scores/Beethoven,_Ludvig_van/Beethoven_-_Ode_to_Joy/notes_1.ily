\key d \major
\tempo Allegro 4=120

fis4 fis g a
a4 g fis e
d4 d e fis
fis4. (e8) e2
\break

fis4 fis g a
a4 g fis e
d4 d e fis
e4. (d8) d2
\break

e4 e fis d
e4 fis8(g8) fis4 d
e4 fis8(g8) fis4 e
d4 e a, r
\break

fis'4 fis g a
a4 g fis e
d4 d e fis
e4. (d8) d2
\bar "|."
