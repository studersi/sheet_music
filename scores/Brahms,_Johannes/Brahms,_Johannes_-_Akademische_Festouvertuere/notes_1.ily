\time 2/4
\partial 4
\key a \major

e4 |
\repeat volta 2 {
	a8 a a8 a |
	a4 b |
	cis8 cis cis8 cis |
	cis4 d |
	e8 e e8 fis |
	\break

	e8 d d4 |
	cis4\staccato (e\staccato) |
	cis8 b b8 e |
	cis8 cis b8 e |
	\break

	cis4 cis |
	b4 b |
}
\alternative {{
	a2 |
	e4\staccato (e\staccato) |
}{
	a2 |
}}
a'4 \bar "|."
