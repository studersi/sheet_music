\time 2/4
\partial 8
\key d \major
\tempo 4=120

a8\upbow |

\repeat volta 2 {
	cis8 cis cis8 b16 a16 |
	e'4. e16 d cis8 cis cis8 b16 a16 |
	e'4. e16 d |
	\break

	cis8 d16 e d8 cis |
}
\alternative {{
	b4. a8\upbow |
}{
	b4. cis8\upbow |
	e8 d16 cis16 d8 e |
	fis8 e4 cis8 |
}}
\break


e8 d16 cis16 d8 e |
fis8 e4 cis8 |
fis8 e4 d8 |
cis8 b16 a16 b4 |
a4. cis8\upbow |
\break

e8 d16 cis16 d8 e |
fis8 e4 cis8 |
e8 d16 cis16 d8 e |
fis8 e4 cis8 |
fis8 e4 d8 |
cis8 b16 a16 b4 |
a4.

\bar "|."
