\repeat volta 2 {
	d2 b4. (c8) |
	d2 g, |
	a8 (b c d) c4 b |
	a1 |
	\break

	b8 ( c d e) d4\tenuto (d4\tenuto) |
	g2 d |
	c4 (b) a4. (g8) |
	g2. r4 |
}
d'8\downbow (a b c) b4 b |
\break

a4 (b8 a) g4 g |
c4 (b) a (g) |
fis1 |
g8 (fis g a) g4 g |
e'2 cis |
\break

d4 (e8 d) cis4. (d8) |
d2. r4 |
d2\downbow b4. (c8) |
d2 g,2 |
a8 (b c d) c4 b |
\break

a1 |
b8 (c d e) d4\tenuto (d4\tenuto) |
g2 d2 |
c4 b8 (a) a4. g8 |
<g g,>1 |

\bar "|."
