\key d \major

d'4 d, e fis |
e4 e a g |
fis4 fis g a |
d4. (cis8) cis2 |

d4 d, e fis |
e4 e a g |
fis4 fis g a |
g4 a fis a |

cis2 d4 a4 |
cis2\upbow d4 a4 |
cis4\downbow r4 fis,2\downbow |
b2\upbow cis4 a |

d4\downbow d, e fis |
e4 e a g |
fis4 fis g a |
g4. (a8) fis2 |

\bar "|."
