\time 4/4
\key c \major
\partial 4

\compressEmptyMeasures
\override MultiMeasureRest.expand-limit = #2

e4 |
g4 b b b |
a4 c b a |
g2. a4 |
b4 g e g |
\break

a4 fis e d |
e2. a4 |
a4 e e fis |
g2 fis4 (e) |
\break

d2. g4 |
a4 b b b |
a4 c b a |
g2. d'4 |
\break

d2. b4 |
b2. b4 |
a4 c b a |
g2. a4 |
b4 g e g |
a4 fis fis d |
e2. r4 |

\bar "|."
