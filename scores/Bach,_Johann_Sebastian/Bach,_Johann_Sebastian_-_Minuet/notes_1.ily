\time 3/4
\key d \major
\tempo Moderato 4=84-96

\repeat volta 2 {
	d8\mf\downbow fis a d e, cis' |
	d4 d,\p (d) |
	d8\mp fis a d e, cis' |
	d4 d, (d) |
	\break

	b'4\mp b b8 d |
	a4 a a8 d |
	g,4 a8 g fis g |
	e4 r2 |
	\break

	d8\mf fis a d e, cis' |
	d4 d,\pp (d) |
	d8\mp fis a d e, cis' |
	d4 d,\p (d) |
	\break

	b'4\mp a8 g fis e |
	a4 g8 fis e d |
	e8 (fis16 g16) fis4\dim (e)\! |
	d4\p r2 |
}
\bar "|."
