\new Staff \with {
	\include "../../../util/lilypond/styles/staff_viola.ily"
}{
	\transpose a g \relative c' {
		\clef alto

		\include "./notes_1.ily"
	}
}
