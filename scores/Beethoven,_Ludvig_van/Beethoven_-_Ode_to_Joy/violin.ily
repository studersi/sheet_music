\new Staff \with {
	\include "../../../util/lilypond/styles/staff_violin.ily"
}{
	\relative c' {
		\clef treble

		\include "./notes_1.ily"
	}
}
\new Staff \with {
	\include "../../../util/lilypond/styles/staff_violin.ily"
}{
	\relative c' {
		\clef treble

		\include "./notes_2.ily"
	}
}
