\time 4/4
\key c \major
\partial 4

\compressEmptyMeasures
\override MultiMeasureRest.expand-limit = #2

c4 |
g'4 g g g |
g4 g g d |
e2. e4 |
e4 e c c |
\break

c4 a c b |
c2. c4 |
c4 c b b |
b2 a4 (c) |
\break

b2. c4 |
c4 g' g g |
g4 g g d |
e2. d4 |
\break

g2. b,4 |
e2. g4 |
g4 g g d |
e2. e4 |
e4 e c c |
c4 a a b |
c2. r4 |

\bar "|."
