a8 (b) |
cis4 cis cis d8 (cis) |
b4 b b cis |
d4 cis8 (b) cis4 b8 (a) |
a2. a8 (b) |
cis4 cis cis d8 (cis) |
\break

b4 b b cis |
d4 cis8 (b) cis4 b8 (a) |
a2. e'4 |
a4 e e e |
fis4 e e e |
e4 fis8 (e) fis8 (e) d8 (cis) |
\break

b2. a8 (b) |
cis4 cis8 (b) cis8 (b) cis8 (a) |
b4 b8 (cis) b4 cis |
d4 cis8 (b) cis4 b8 (a) |
a2. e'4 |
a4 e e e |
\break

fis4 e e e |
e4 fis8 (e) fis8 (e) d8 (cis) |
b2. a8 (b) |
cis4 b8 (cis) d8 (cis) b8 (a) |
b8 (a) b8 (cis) b4 cis |
d4 cis8 (b) cis4 b8 (a) |

a2.
\bar "|."
