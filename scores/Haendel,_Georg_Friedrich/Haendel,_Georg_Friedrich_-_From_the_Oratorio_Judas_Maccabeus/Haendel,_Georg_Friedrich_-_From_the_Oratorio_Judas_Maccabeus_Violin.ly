\version "2.18.2"
\include "../../../util/lilypond/styles/paper_default.ily"

\bookpart {
	\include "./header.ily"
	\score {
		\layout{}
		\midi{
			\tempo 4 = 120
		}
		<<
		\new Staff \with {
			\include "../../../util/lilypond/styles/staff_violin.ily"
		}{
			\relative c'' {
				\clef treble
				\tempo "Tempo di marcia"
				\key g \major

				\include "./notes_1.ily"
			}
		}

		\new Staff \with {
			\include "../../../util/lilypond/styles/staff_violin.ily"
		}{
			\relative c'' {
				\clef treble
				\key g \major

				\include "./notes_2.ily"
			}
		}
		>>
	}
}
