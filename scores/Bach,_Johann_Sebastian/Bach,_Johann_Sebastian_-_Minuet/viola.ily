\new Staff \with {
	\include "../../../util/lilypond/styles/staff_viola.ily"
}{
	\transpose c f \relative c {
		\clef alto

		\include "./notes_1.ily"
	}
}
