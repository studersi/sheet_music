\version "2.18.2"
\include "../../../util/lilypond/styles/paper_default.ily"

\bookpart {
	\include "./header.ily"
	\score {
		\layout{}
		\midi{
			\tempo 4 = 88
		}
		<<
		\new Staff \with {
			\include "../../../util/lilypond/styles/staff_violin.ily"
		}{
			\relative c'' {
				\clef treble
				\time 4/4
				\key a \major
				\partial 4

				\include "./notes_1.ily"
			}
		}

		\new Staff \with {
			\include "../../../util/lilypond/styles/staff_violin.ily"
		}{
			\relative c'' {
				\clef treble
				\time 4/4
				\key a \major
				\partial 4

				\include "./notes_2.ily"
			}
		}

		\new Staff \with {
			\include "../../../util/lilypond/styles/staff_viola.ily"
		}{
			\relative c' {
				\clef alto
				\time 4/4
				\key a \major
				\partial 4

				\include "./notes_1.ily"
			}
		}

		\new Staff \with {
			\include "../../../util/lilypond/styles/staff_viola.ily"
		}{
			\relative c'' {
				\clef alto
				\time 4/4
				\key a \major
				\partial 4

				\include "./notes_2.ily"
			}
		}
		>>
	}
}
