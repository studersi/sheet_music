\time 3/4
\key c \major
\tempo "Dolce espressivo" 4=77

g2. |
d'4 (c2) |
b4. a8 (g f) |
g2 c,4 |
b'4. a8 (g f) |
\break

g2 c,4 |
d2. ~|
d2 e4 |
c2. |
g'4 e4. (d8) |
\break

c2. |
g'4 e4. (d8) |
c4 e4 g4 |
c4 b4 a4 |
\break

b4 a4. (g8) |
a2. |
g2. |
d'4 (c2) |
b4. a8 (g f) |
g2 c,4 |
\break

b'4. a8 (g f) |
g2 c,4 |
d2. ~ |
d2 e4 |
c2. |
e2 g4 |
c2. |
\break

d2 c4 |
g2. ~ |
g4 b8 a g f |
g2. ~ |
g8 c, b'8 a g f |
\break

g2. |
c,2. |
f4 d2 |
d2 e4 |
c2. ~ |
c2. |

\bar "|."
