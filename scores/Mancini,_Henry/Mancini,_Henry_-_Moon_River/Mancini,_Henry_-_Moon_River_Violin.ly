\version "2.18.2"
\include "../../../util/lilypond/styles/paper_default.ily"

\bookpart {
	\include "./header.ily"
	\score {
		\layout{}
		\midi{
			\tempo 4 = 77
		}
		<<
		\include "./violin.ily"
		>>
	}
}
