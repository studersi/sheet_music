\time 4/4
\key f \major
\tempo Allegro 4=100

\repeat volta 2 {
	a'8\f\downbow a a bes a4 a8\upbow bes\upbow |
	a8 a a bes a4 a8\upbow bes\upbow |
	a8 g16 (a16 bes8) a g4 r4 |
	a,8\p a a bes a4 a8\upbow bes\upbow |
	\break

	a8 a a bes a4 a8\upbow bes\upbow |
	a8 g16 (a16 bes8) a g4 r4 |
	a'8\f a a bes d,4 d8\upbow d\upbow |
	g8 g g bes c,4 c8\upbow f\upbow |
	\break

	a8 a a bes a8 a a bes |
	a8 a a bes a8 g16 (a16 bes8) g8 |
	a4 r4 a,8\p a a bes |
	a8 a a bes a a a bes |
	\break

	a8 g16 (a16 bes8) g f4 r4 |
	a'8\f a a bes a4 a8\upbow bes\upbow |
	a8 a a bes a4 a8\upbow bes\upbow |
	a8 g16 (a16 bes8) a g4 r4 |
	\break

	a8\p a a bes a4 a8\upbow bes\upbow |
	a8 a a bes a4 a8\upbow bes\upbow |
	a8 g16 (a16 bes8) a g4 r4 |
}

\repeat volta 2 {
	f8\p c c c d4 c8\upbow c\upbow |
	f8 c c c d4 c8\upbow c\upbow |
	\break

	d4 c8\upbow c\upbow f8 e16 (d16 c8) bes |
	c4 bes4 a8\f a' a bes |
	a8 a a bes a8 a a bes |
	a8 g16 (a16 bes8) g a4 r4 |
	\break

	a,8\p a a bes a8 a a bes |
	a8 a a bes a8 g16 (a16 bes8) g |
}

f4

\bar "|."
